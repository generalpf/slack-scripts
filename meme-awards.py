import requests
import json
import re
from datetime import datetime


token = ''
# no pound before channel name
channel = 'memes'
days_to_scan = 7


def flatten(thing):
    result = []
    for key in thing.keys():
        value = thing[key]
        if type(value) is dict:
            value['id'] = key
            result.append(value)
        else:
            result.append({
                'id': key,
                'value': value,
            })
    return result


def channel_id_for_name(name):
    params = {
        'token': token,
        'exclude_archived': True,
        'exclude_members': True,
        'types': 'public_channel',
    }

    while True:
        r = requests.get('https://slack.com/api/conversations.list', params=params)
        channels = json.loads(r.text)
        if not channels['ok']:
            print(channels['error'])
            return None
        for channel in channels['channels']:
            if channel['name'] == name:
                return channel['id']
        params['cursor'] = channels['response_metadata']['next_cursor']
        # TODO: something if there's no next cursor


def stats_for_channel(name):
    channel_id = channel_id_for_name(name)

    words_file = open('words.' + name + '.txt', 'w')

    user_stats = {}
    reaction_stats = {}
    post_stats = {}

    def ensure_user(userid):
        if userid not in user_stats:
            user_stats[userid] = {
                'post_count': 0,
                'reactions_received': 0,
                'reactions_posted': 0,
            }

    def ensure_reaction(name):
        if name not in reaction_stats:
            reaction_stats[name] = 0

    params = {
        'token': token,
        'channel': channel_id,
    }
    while True:
        r = requests.get('https://slack.com/api/conversations.history', params=params)
        messages = json.loads(r.text)
        if not messages['ok']:
            print("couldn't retrieve messages: " + r.text)
            return (None, None, None)

        for message in messages['messages']:
            words_file.write(clean_words(message['text']) + '\n')

            ts = int(float(message['ts']))
            d = datetime.fromtimestamp(ts)
            if (datetime.now() - d).days >= days_to_scan:
                print("ending at " + str(d))
                words_file.close()
                return flatten(user_stats), flatten(reaction_stats), flatten(post_stats)

            content = extract_content(message)
            if content is None:
                continue

            ensure_user(message['user'])
            user_stats[message['user']]['post_count'] += 1
            new_post_stats = {
                'user': message['user'],
                'reactions': 0,
                'replies': message['reply_count'] if 'reply_count' in message else 0,
            }
            reaction_count = 0
            if 'reactions' in message:
                for reaction in message['reactions']:
                    ensure_reaction(reaction['name'])
                    reaction_count += len(reaction['users'])
                    reaction_stats[reaction['name']] += len(reaction['users'])
                    for userid in reaction['users']:
                        ensure_user(userid)
                        if userid != message['user']:
                            user_stats[message['user']]['reactions_received'] += 1
                            user_stats[userid]['reactions_posted'] += 1
            new_post_stats['reactions'] = reaction_count
            post_stats[content] = new_post_stats
        params['cursor'] = messages['response_metadata']['next_cursor']
        print("next cursor: " + params['cursor'])


def clean_words(message):
    if 'Top 5 posters by quality' in message:
        return ''

    foo = message
    foo = re.sub('<@.*>', '', foo)
    foo = re.sub('<http.*>', '', foo)
    return foo


def extract_content(message):
    if message['type'] != 'message' or 'user' not in message:
        return None
    if 'files' in message and len(message['files']) != 0 and \
            'permalink' in message['files'][0]:
        return message['files'][0]['permalink']
    if 'twitter' in message['text']:
        m = re.search('<(.*twitter.*)>', message['text'])
        if m is not None:
            return m.group(1)
    return None


def fill_in_users(user_stats):
    for user_stat in user_stats:
        params = {
            'token': token,
            'user': user_stat['id'],
        }
        r = requests.get('https://slack.com/api/users.info', params=params)
        user = json.loads(r.text)
        if user['ok']:
            user_stat['name'] = user['user']['profile']['display_name']


(user_stats, reaction_stats, post_stats) = stats_for_channel(channel)
fill_in_users(user_stats)
# print(user_stats)
# print(reaction_stats)
# print(post_stats)

print("Top 5 posters by quality (reactions received / post count) (three post minimum)")
top_5_quality = sorted([u for u in user_stats if u['post_count'] >= 3], key=lambda k: k['reactions_received'] / k['post_count'], reverse=True)[:5]
for v in top_5_quality:
    print("{0:.3} from @{1}".format(v['reactions_received'] / v['post_count'], v['name']))
print()

print("Top 5 posters by volume:")
top_5_volume = sorted(user_stats, key=lambda k: k['post_count'], reverse=True)[:5]
for v in top_5_volume:
    print("{0} from @{1}".format(v['post_count'], v['name']))
print()

print("Top 5 reaction-getters:")
top_5_getters = sorted(user_stats, key=lambda k: k['reactions_received'], reverse=True)[:5]
for v in top_5_getters:
    print("{0} for @{1}".format(v['reactions_received'], v['name']))
print()

print("Top 5 reaction-givers:")
top_5_givers = sorted(user_stats, key=lambda k: k['reactions_posted'], reverse=True)[:5]
for v in top_5_givers:
    print("{0} from @{1}".format(v['reactions_posted'], v['name']))
print()

print("Top 5 reactions:")
top_5_reactions = sorted(reaction_stats, key=lambda k: k['value'], reverse=True)[:5]
for v in top_5_reactions:
    print(":{0}: x{1}".format(v['id'], v['value']))
print()

print("Top 5 posts by reaction count:")
top_5_posts = sorted(post_stats, key=lambda k: k['reactions'], reverse=True)[:5]
for v in top_5_posts:
    user = [u for u in user_stats if u['id'] == v['user']]
    print("{0} {1} by @{2}".format(v['reactions'], v['id'], user[0]['name']))
print()

# print("Top 5 (or fewer) posts by reply count:")
# top_5_posts = sorted(post_stats, key=lambda k: k['replies'], reverse=True)[:5]
# for v in top_5_posts:
#     user = [u for u in user_stats if u['id'] == v['user']]
#     if v['replies'] > 0:
#         print("{0} {1} by @{2}".format(v['replies'], v['id'], user[0]['name']))
# print()

print("Top 5 (or fewer) cheapskates (reactions received - given): ")
top_5_cheapskates = sorted(user_stats, key=lambda k: k['reactions_received'] - k['reactions_posted'], reverse=True)[:5]
for v in top_5_cheapskates:
    if v['reactions_received'] - v['reactions_posted'] > 5:
        print("{0} from @{1}".format(v['reactions_received'] - v['reactions_posted'], v['name']))
