import requests
import json
from datetime import datetime


token = ''

params = {
    'token': token,
    'ts_from': '1420131600',
    'ts_to': '1538323200',
    'page': 1,
    'types': 'images,videos',
}

r = requests.get('https://slack.com/api/files.list', params=params)
response = json.loads(r.text)
if response['ok']:
    print("there are {0} files left in {1} pages".format(response['paging']['total'], response['paging']['pages']))
    for file in response['files']:
        dparams = {
            'token': token,
            'file': file['id'],
        }
        dr = requests.get('https://slack.com/api/files.delete', params=dparams)
        dresponse = json.loads(dr.text)
        print(dresponse['ok'])
else:
    print(response)
